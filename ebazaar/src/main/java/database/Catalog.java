package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Catalog {

	public static List<String[]> catalogs() {
		List<String[]> theData = new ArrayList<String[]>();

		try {
        	System.out.println("using database");
        	Statement stmt = Database.connect("ProductsDb");
        	String insertStmt = "";
        	String selectStmt = "";
        	
			ResultSet rs = stmt.executeQuery("SELECT * FROM CatalogType");
			while(rs.next()){
				String id = rs.getString("catalogid");
				String name = rs.getString("catalogname");
				System.out.println("id: "+ id + " name: "+name);
				theData.add(new String[]{name});
			}
			stmt.close();
			Database.close();
		}
		catch(SQLException | ClassNotFoundException s){
			s.printStackTrace();
		}
		
		return theData;
	}

}
