package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Order {
	
	public static List<String> getAllOrderIds(String custId) {
		List<String> theData = new ArrayList<String>();

		try {
        	Statement stmt = Database.connect("AccountsDb");
        	String selectStmt = "select orderid from Ord where custid="+custId;
        	System.out.println(selectStmt);
			ResultSet rs = stmt.executeQuery(selectStmt);
			while(rs.next()){
				theData.add(rs.getString("orderid"));
			}
			stmt.close();
			Database.close();
		}
		catch(SQLException | ClassNotFoundException s){
			s.printStackTrace();
		}
		
		System.out.println("data : " + theData.size());
		return theData;
	}
	
	public static String[] getOrderData(String orderId) {
		String[] theData = new String[3];

		try {
        	Statement stmt = Database.connect("AccountsDb");
        	String selectStmt = "select orderid, orderdate, totalpriceamount from Ord where orderid="+orderId;
        	System.out.println(selectStmt);
			ResultSet rs = stmt.executeQuery(selectStmt);
			while(rs.next()){
				theData = new String[]{rs.getString("orderid"), rs.getString("orderdate"), "$" + rs.getString("totalpriceamount")};
			}
			stmt.close();
			Database.close();
		}
		catch(SQLException | ClassNotFoundException s){
			s.printStackTrace();
		}
		
		System.out.println("data : " + theData.length);
		return theData;
	}
	
	public static List<String[]> getOrderProducts(String orderId) {
		List<String[]> theData = new ArrayList<>();

		try {
        	Statement stmt = Database.connect("AccountsDb");
        	String selectStmt = "select oi.orderid,p.productid,p.productname,p.priceperunit, oi.quantity, oi.totalprice "
        			+ "from AccountsDb.Ord o INNER JOIN AccountsDb.OrderItem oi on o.orderid = oi.orderid "
        			+ "Inner Join ProductsDb.Product p on oi.productid = p.productid where o.orderid="+orderId;
        	System.out.println(selectStmt);
			ResultSet rs = stmt.executeQuery(selectStmt);
			while(rs.next()){
				String[] data = new String[]{rs.getString("productname"), rs.getString("quantity"), rs.getString("priceperunit"), rs.getString("totalprice")};
				theData.add(data);
			}
			stmt.close();
			Database.close();
		}
		catch(SQLException | ClassNotFoundException s){
			s.printStackTrace();
		}
		
		System.out.println("data : " + theData.size());
		return theData;
	}
}
