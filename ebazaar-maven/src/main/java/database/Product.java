package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Product {
	
	public static List<String[]> products(String catalogId) {
		List<String[]> theData = new ArrayList<String[]>();

		try {
        	System.out.println("using database");
        	Statement stmt = Database.connect("ProductsDb");
        	String selectStmt = "";
        	
			ResultSet rs = stmt.executeQuery("select p.productid as productid, p.productname as productname from Product p "
					+ "join CatalogType c on p.catalogid = c.catalogid where c.catalogname=\""+catalogId+"\"");
			while(rs.next()){
				String id = rs.getString("productid");
				String name = rs.getString("productname");
				System.out.println(id + " : "+name);
				theData.add(new String[]{name});
			}
			stmt.close();
			Database.close();
		}
		catch(SQLException | ClassNotFoundException s){
			s.printStackTrace();
		}
		
		return theData;
	}
	
	public static List<String> details(String type) {
		List<String> data = new ArrayList<String>();

		System.out.println("Using db for ProductDetailsList");
		System.out.println("ProductType = " + type);
		
		try {

			Statement statement = Database.connect("ProductsDb");
			ResultSet rs = statement
					.executeQuery("SELECT productname, priceperunit, totalquantity, description from Product"
							+ " where productname = \"" + type + "\"");
			while (rs.next()) {
				String aProductName = rs.getString("productname");
				String priceperunit = rs.getString("priceperunit");
				String totalquantity = rs.getString("totalquantity");
				String description = rs.getString("description");
				data.add(aProductName);
				data.add(priceperunit);
				data.add(totalquantity);
				data.add(description);
			}
			statement.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return data;
	}
}
