package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
	public static final String JDBC = "jdbc:mysql://localhost/";
	public static final String PARAM = "?serverTimezone=UTC";

	public static final String driver1 = "com.mysql.jdbc.Driver";
	public static final String driver2 = "org.gjt.mm.mysql.Driver";
	
	static Connection con = null;
	
	public static Statement connect(String database) throws SQLException, ClassNotFoundException {
	
    	Statement stmt = null;
    	
		Class.forName(driver2);
	
		con = DriverManager.getConnection(JDBC + database + PARAM, "root", "");
		
		stmt = con.createStatement();
		
		return stmt;
	}
	
	public static void close() throws SQLException {
		con.close();
	}
}
