Start mysql
-----------

```
mysql.server start
```


clean and compile using maven
-----------------------------

add dependencies

```bash
mvn install:install-file \
   -Dfile=lib/creditVerifSystem.jar \
   -DgroupId=com.credit.verification \
   -DartifactId=credit-verification \
   -Dversion=1.0 \
   -Dpackaging=jar \
   -DgeneratePom=true


mvn install:install-file \
   -Dfile=lib/jess.jar \
   -DgroupId=com.jess \
   -DartifactId=jess \
   -Dversion=1.0 \
   -Dpackaging=jar \
   -DgeneratePom=true


mvn install:install-file \
   -Dfile=lib/rulesencoding.jar \
   -DgroupId=com.rulesencoding \
   -DartifactId=rulesencoding \
   -Dversion=1.0 \
   -Dpackaging=jar \
   -DgeneratePom=true


mvn install:install-file \
   -Dfile=lib/rulesengine.jar \
   -DgroupId=com.rulesengine \
   -DartifactId=rulesengine \
   -Dversion=1.0 \
   -Dpackaging=jar \
   -DgeneratePom=true

```

success message would look something like this

```
$ mvn install:install-file \
→    -Dfile=rulesengine.jar \
→    -DgroupId=com.rulesengine \
→    -DartifactId=rulesengine \
→    -Dversion=1.0 \
→    -Dpackaging=jar \
→    -DgeneratePom=true
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-install-plugin:2.4:install-file (default-cli) @ standalone-pom ---
[INFO] Installing /Users/prayagupd/prayag.data/workspace.programming/java8/westerville-workspace/main_code/externalJars/rulesengine.jar to /Users/prayagupd/.m2/repository/com/rulesengine/rulesengine/1.0/rulesengine-1.0.jar
[INFO] Installing /var/folders/63/jvvb4wy16gx6w76mkgsvn52m0000gn/T/mvninstall1931601182273472229.pom to /Users/prayagupd/.m2/repository/com/rulesengine/rulesengine/1.0/rulesengine-1.0.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 0.324 s
[INFO] Finished at: 2016-10-22T21:34:43-05:00
[INFO] Final Memory: 7M/245M
[INFO] ------------------------------------------------------------------------
```

```
mvn clean compile
```

change the database config if needed
--------------------------------------

```
resources/dbconfig.properties
```

build the artifact
------------------

```
mvn install
```

jar will be in target/


run using maven
---------------

```
mvn exec:java -Dexec.mainClass="application.gui.EbazaarMainFrame"
```
